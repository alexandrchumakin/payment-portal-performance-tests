# Payment Portal Performance Tests
Performance tests with JMeter for Payment Portal built with Dark language. 
Canvas can be found [here](https://darklang.com/a/alexandrchumakin-payment-app).

More information about the whole solution could be found in [portfolio page](https://alexandrchumakin.github.io) under `Performance tests with JMeter` block.

## Local setup
- JDK 16
- JMeter

## Local execution
In order to have clean result, tests should be executed via non-GUI mode with disabled debug samplers and listeners. 
You could enable it manually for local debugging in JMeter IDE to see the output like this: 
![jmeter-listener-debug](./docs/media/jmeter-listener-debug.png)

### Run tests in CLI
- check JMeter CLI installation with: `jmeter -v`
- to generate jtl-file output execute: `jmeter -n -t payment-portal-tests.jmx -l result.jtl`
- to generate HTML dashboard run: `jmeter -n -t payment-portal-tests.jmx -l result.jtl -e -o ./dashboard`

With the last command JMeter CLI will run tests and generate nice dashboard that will have a lot of details about tests execution.
![dashboard-stats](./docs/media/dashboard-stats.png)
![dashboard-response-time](./docs/media/dashboard-response-time.png)
